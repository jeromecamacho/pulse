<?php

use SilverStripe\ORM\DataObject;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;


use AboutUsPage;

class Gallery extends DataObject{

	private static $singular_name = "Employee";

	private static $db = [
		'dbName' => 'Text',
		'dbPosition' => 'Text',
	];

	private static $has_one = [
		'dbImage' => Image::class,

        'AboutUsPage' => AboutUsPage::class

	];

	private static $owns = [
		'dbImage'
	];


	public function getGridThumbnail()
    {
        if($this->dbImage()->exists()) {
            return $this->dbImage()->ScaleWidth(100)->ScaleHeight(100);
        }
        return "( No image )";
    }


    private static $summary_fields = [
        'GridThumbnail' => 'Photo',
        'dbName' => 'Name',
        'dbPosition' => 'Position',
    ];

	public function getCMSFields(){

		$fields = FieldList::create(
            TextField::create('dbName','Name'),
            TextField::create('dbPosition','Position'),
            $uploader = UploadField::create('dbImage','Image')->setDescription("Only png, jpeg, jpg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 450px and height: 450px)")
        );

        $uploader->setFolderName('Uploads/Gallery_Photos');
        $uploader->getValidator()->setAllowedExtensions(['png','jpeg','jpg']);
        $uploader->getValidator()->setMinDimensions(450,450);
        $uploader->setAllowedMaxFileNumber(1);

        return $fields;

	}




}