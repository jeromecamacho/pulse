<?php

class AboutUsPageController extends PageController{


	public function dbLabel(){

		if(trim($this->dbLabel) != null){

			return $this->dbLabel;

		}

		return "About Pulse";

	}


	public function dbDetails(){

		if(trim($this->dbDetails) != null){

			return $this->dbDetails;

		}

		return "After over a decade of creating custom web and mobile applications for small business and enterprise clients, the Pulse team saw how we could provide a feature-rich patient engagement experience without the high costs and time associated with the typical development cycle. By combining commonly used features along with a proprietary system that creates unique, secure instances for each client, we are able to level up your business in an unprecedented way.";

	}


	public function dbDetails2(){

		if(trim($this->dbDetails2) != null){

			return $this->dbDetails2;

		}

		return "Our design team lives and breathes User Experience. Our Developers are Full Stack and committed to smooth and stable experiences for our clients and your users. Our Product Managers and Strategists have responded with solutions to issues so that you won’t have to."."\n"."\n"."At Pulse, we are constantly building more features into the platform, so you can focus on your customers, and they can stay focused on you.";
	}


	public function dbLabelTheHeart(){

		if(trim($this->dbLabelTheHeart) != null){

			return $this->dbLabelTheHeart;

		}

		return "The heart behind the Pulse";

	}



	public function dbDetailsTheHeart(){

		if(trim($this->dbDetailsTheHeart) != null){

			return $this->dbDetailsTheHeart;
			
		}

		return "We are a diverse team of strategists, designers and developers."."\n"."Our expertise lets us build a robust, polished apps that let your business create new and meaningful user experiences. ";

	}


	public function labelCTACaption(){


		if(trim($this->labelCTACaption) != null){

			return $this->labelCTACaption;

		}

		return "Ready to have your own iOS and Android app?";

	}

	public function labelCTAButtonLabel(){

		if(trim($this->labelCTAButtonLabel) != null){

			return $this->labelCTAButtonLabel;

		}

		return "Talk to us";

	}


	public function labelCTAButtonLink(){

		if(trim($this->labelCTAButtonLink) != null){

			return $this->labelCTAButtonLink;

		}

		return "#";

	}



}