<?php

use SilverStripe\Forms\TextField;

class FooterPage extends Page{

	
	private static $db = [
		'dbEmail' => 'Text',
		'dbAddress' => 'Text',
		'dbPhone' => 'Text',
		'dbCopyrightDetails' => 'Text',
		'dbFacebookLink' => 'Text',
		'dbTwitterLink' => 'Text',
		'dbInstagramLink' => 'Text'
	];

	public function getCMSFields(){

		$fields = parent::getCMSFields();

		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('dbEmail','Email'),
			'Content'
		);

		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('dbAddress','Address'),
			'Content'
		);

		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('dbPhone','Phone'),
			'Content'
		);

		

		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('dbCopyrightDetails','Copyright Details'),
			'Content'
		);


		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('dbFacebookLink','Facebook Link')->setDescription("http://sample.com"),
			'Content'
		);

		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('dbTwitterLink','Twitter Link')->setDescription("http://sample.com"),
			'Content'
		);

		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('dbInstagramLink','Instagram Link')->setDescription("http://sample.com"),
			'Content'
		);



		return $fields;

	}




}