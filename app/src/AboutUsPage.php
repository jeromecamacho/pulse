<?php

use SilverStripe\Forms\TextField;

use SilverStripe\Forms\TextareaField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;


use Gallery;

class AboutUsPage extends Page{


	private static $db = [

		// about pulse
		'dbLabel' => 'Text',
		'dbDetails' => 'Text',
		'dbDetails2' => 'Text',

		// Gallery Section
		'dbLabelTheHeart' => 'Text',
		'dbDetailsTheHeart' => 'Text',

		// CTA
		'labelCTACaption' => 'Text',
		'labelCTAButtonLabel' => 'Text',
		'labelCTAButtonLink' => 'Text'
	];

	private static $has_one = [

		// header 
		'imageBackgroundHeader' => Image::class,

		// about pulse
		'imageAboutIcon' => Image::class,
		'imageBigImage' => Image::class,

		// CTA
		'imageIconCTA' => Image::class,
		'imageBackgroundCTA' => Image::class,

	];

	private static $has_many = [
		"Gallery" => Gallery::class,
	];

	private static $owns = [
		'imageBackgroundHeader',
		'imageAboutIcon',
		'imageBigImage',

		'imageIconCTA',
		'imageBackgroundCTA',

	];

	public function getCMSFields(){

		$fields = parent::getCMSFields();

		// header
		$fields->addFieldToTab(
            'Root.Header Section',
            $imageBackgroundHeader = UploadField::create('imageBackgroundHeader','Background Banner')->setDescription("Only png, jpeg, jpg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 2000px and height: 1000px)"),
            ''
        );
        $imageBackgroundHeader->getValidator()->setAllowedExtensions(['png','jpeg','jpg']);
        $imageBackgroundHeader->getValidator()->setMinDimensions(2000,1000);
        $imageBackgroundHeader->setAllowedMaxFileNumber(1);
        $imageBackgroundHeader->setFolderName('Uploads/imageBackgroundHeader');


        // about pulse
        $fields->addFieldToTab(
            'Root.About Pulse Section',
            $imageAboutIcon = UploadField::create('imageAboutIcon','Icon')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 138px and height: 138px)"),
            ''
        );
        $imageAboutIcon->getValidator()->setAllowedExtensions(['png']);
        $imageAboutIcon->getValidator()->setMinDimensions(138,138);
        $imageAboutIcon->setAllowedMaxFileNumber(1);
        $imageAboutIcon->setFolderName('Uploads/imageAboutIcon');


		$fields->addFieldToTab(
			'Root.About Pulse Section',
			TextField::create('dbLabel','Label')
		);



		$fields->addFieldToTab(
	    	'Root.About Pulse Section',
	    	TextareaField::create('dbDetails','Details')->setRows(3)
        );


        $fields->addFieldToTab(
            'Root.About Pulse Section',
            $imageBigImage = UploadField::create('imageBigImage','Big Image')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1152px and height: 436px)"),
            ''
        );
        $imageBigImage->getValidator()->setAllowedExtensions(['png']);
        $imageBigImage->getValidator()->setMinDimensions(1152,436);
        $imageBigImage->setAllowedMaxFileNumber(1);
        $imageBigImage->setFolderName('Uploads/imageBigImage');

        
        $fields->addFieldToTab(
	    	'Root.About Pulse Section',
	    	TextareaField::create('dbDetails2','Details 2')->setRows(3)
        );

		// Gallery Section
        $fields->addFieldToTab(
			'Root.Gallery Section',
			TextField::create('dbLabelTheHeart','Label')
		);


		$fields->addFieldToTab(
	    	'Root.Gallery Section',
	    	TextareaField::create('dbDetailsTheHeart','Details')->setRows(3)
        );


        $fields->addFieldToTab(
            'Root.Gallery Section',
            GridField::create('Gallery','List of Employees',$this->Gallery(),GridFieldConfig_RecordEditor::create()),
            ''
        );



        // CTA
   	 	$fields->addFieldToTab(
            'Root.CTA Section',
            $imageIconCTA = UploadField::create('imageIconCTA','Header Background')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 63px and height: 68px)"),
            ''
        );
        $imageIconCTA->getValidator()->setAllowedExtensions(['png']);
        $imageIconCTA->getValidator()->setMinDimensions(63,68);
        $imageIconCTA->setAllowedMaxFileNumber(1);
        $imageIconCTA->setFolderName('Uploads/imageIconCTA');



   	 	$fields->addFieldToTab(
			'Root.CTA Section',
   	 		TextField::create('labelCTACaption',"Label")->setDescription("Maximum of 30 characters including spaces.")->setMaxLength(30),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.CTA Section',
   	 		TextField::create('labelCTAButtonLabel',"Button Label")->setDescription("Maximum of 30 characters including spaces.")->setMaxLength(30),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.CTA Section',
   	 		TextField::create('labelCTAButtonLink',"Redirect To")->setDescription("http://sample.com"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.CTA Section',
            $imageBackgroundCTA = UploadField::create('imageBackgroundCTA','Header Background')->setDescription("Only png, jpeg, jpg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 2000px and height: 1000px)"),
            ''
        );
        $imageBackgroundCTA->getValidator()->setAllowedExtensions(['png','jpeg','jpg']);
        $imageBackgroundCTA->getValidator()->setMinDimensions(2000,1000);
        $imageBackgroundCTA->setAllowedMaxFileNumber(1);
        $imageBackgroundCTA->setFolderName('Uploads/imageBackgroundCTA');




		return $fields;

	}

}