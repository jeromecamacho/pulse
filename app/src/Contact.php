<?php

use SilverStripe\ORM\DataObject;

use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;

use ContactPage;

class Contact extends DataObject{

	private static $db = [
		'dbFirstname' => 'Text',
		'dbLastname' => 'Text',
		'dbEmail' => 'Text',
		'dbPhone' => 'Text',
		'dbCompany' => 'Text',
		'dbComments' => 'Text'
	];

	private static $has_one = [
		'ContactPage' => ContactPage::class
	];

	private static $summary_fields = [
		"dbFirstname" => "FirstName",
		'dbLastname' => 'LastName',
		'dbEmail' => 'Email',
		'dbPhone' => 'Phone',
		'dbCompany' => 'Company',
		'dbComments' => 'Comments',
	];

	public function getCMSFields(){

		$fields = FieldList::create(
            TextField::create('dbFirstname','First Name'),
            TextField::create('dbLastname','Last Name'),
            TextField::create('dbEmail','Email'),
            TextField::create('dbPhone','Phone'),
            TextField::create('dbCompany','Company Name'),
            TextField::create('dbComments','Comments')
        );

		return $fields;

	}

}