<?php

namespace Main;

use PageController;

class HomePageController extends PageController{


	public function caption1(){

		if(trim($this->caption1) != null){

			return wordwrap($this->caption1,20,"\n",true);

		}

		return "Mobile App Solutions for Healthcare Providers";

	}


	public function caption2(){

		if(trim($this->caption2) != null){

			return wordwrap($this->caption2,50,"\n",true);

		}

		return "Our simple to use software and mobile app provide a seamless solution for your visitors and staff.";

	}

	public function buttonCaptionHeader(){

		if(trim($this->buttonCaptionHeader) != null){

			return $this->buttonCaptionHeader;

		}

		return "tell me more";


	}


	public function label1_weFocus(){

		if(trim($this->label1_weFocus) != null){

			return $this->label1_weFocus;

		}

		return "We Focus on Patient Engagement";


	}

	public function label2_weFocus(){


		if(trim($this->label2_weFocus) != null){

			return $this->label2_weFocus;

		}

		return "Pulse allows Healthcare Organizations to communicate with their members via iOS and Android app.";

	}


	public function label1PushNotif(){

		if(trim($this->label1PushNotif) != null){

			return $this->label1PushNotif;

		}

		return "Push Notifications";


	}

	public function details1PushNotif(){


		if(trim($this->details1PushNotif) != null){

			return $this->details1PushNotif;

		}

		return "Easily keep your patients informed and engaged with instant or scheduled push notifications";

	}

	public function label2PushNotif(){


		if(trim($this->label2PushNotif) != null){

			return $this->label2PushNotif;

		}

		return "GeoFence Notifications";

	}


	public function details2PushNotif(){

		if(trim($this->details2PushNotif) != null){

			return $this->details2PushNotif;

		}

		return "Welcome your patients as they arrive in your parking lot";

	}

	public function labelRealTimeAppointments(){

		if(trim($this->labelRealTimeAppointments) != null){

			return $this->labelRealTimeAppointments;
		}

		return "Real Time Appointments";

	}

	public function detailRealTimeAppointments(){

		if(trim($this->detailRealTimeAppointments) != null){

			return $this->detailRealTimeAppointments;
		}

		return "Patients can request and/or schedule appointments in real time based on your availability";

	}


	public function labelPulseDashboard(){

		if(trim($this->labelPulseDashboard) != null){

			return $this->labelPulseDashboard;

		}

		return "Pulse Dashboard";

	}

	public function detailsPulseDashboard(){

		if(trim($this->detailsPulseDashboard) != null){

			return $this->detailsPulseDashboard;

		}

		return "Easily update the content on your app in real time using a user-friendly app management web dashboard. ";

	}


	public function label2PulseDashboard(){

		if(trim($this->label2PulseDashboard) != null){

			return $this->label2PulseDashboard;

		}

		return "No coding skills required.";

	}

	public function label1PatientCheckin(){

		if(trim($this->label1PatientCheckin) != null){

			return $this->label1PatientCheckin;

		}

		return "PulseMVP Only";

	}


	public function label2PatientCheckin(){

		if(trim($this->label2PatientCheckin) != null){

			return $this->label2PatientCheckin;

		}

		return "Patient Check-in";

	}


	public function detailsPatientCheckin(){

		if(trim($this->detailsPatientCheckin) != null){

			return $this->detailsPatientCheckin;

		}

		return "Mobile app users can simply check in and fill up registration forms using iBeacon technology";

	}


	public function label3PatientCheckin(){

		if(trim($this->label3PatientCheckin) != null){

			return $this->label3PatientCheckin;

		}

		return "iBeacons";

	}

	public function details2PatientCheckin(){

		if(trim($this->details2PatientCheckin) != null){

			return $this->details2PatientCheckin;

		}

		return "iBeacons can also welcome and instruct your visitors as they navigate through your facility";

	}

	


	public function label1EClipboard(){


		if(trim($this->label1EClipboard) != null){

			return $this->label1EClipboard;

		}

		return "PulseMVP Only";

	}


	public function label2EClipboard(){


		if(trim($this->label2EClipboard) != null){

			return $this->label2EClipboard;

		}

		return "Electronic Clipboard";


	}

	public function detailsEClipboard(){


		if(trim($this->detailsEClipboard) != null){

			return $this->detailsEClipboard;

		}

		return "No mobile app?  No problem!  Patients can easily check in by utilizing the Pulse Electronic Clipboard.";


	}

	public function labelAmazingFeatures(){

		if(trim($this->labelAmazingFeatures) != null){

			return $this->labelAmazingFeatures;

		}

		return "Other Amazing Features";

	}



	public function labelCard1AmazingFeatures(){

		if(trim($this->labelCard1AmazingFeatures) != null){

			return $this->labelCard1AmazingFeatures;

		}

		return "Featured Promotions";

	}

	public function labelCard2AmazingFeatures(){

		if(trim($this->labelCard2AmazingFeatures) != null){

			return $this->labelCard2AmazingFeatures;

		}

		return "HIPAA Compliant";

	}

	public function labelCard3AmazingFeatures(){

		if(trim($this->labelCard3AmazingFeatures) != null){

			return $this->labelCard3AmazingFeatures;

		}

		return "iBeacon Technology";

	}

	public function labelCard4AmazingFeatures(){

		if(trim($this->labelCard4AmazingFeatures) != null){

			return $this->labelCard4AmazingFeatures;

		}

		return "Video";

	}

	public function labelCard5AmazingFeatures(){

		if(trim($this->labelCard5AmazingFeatures) != null){

			return $this->labelCard5AmazingFeatures;

		}

		return "Event Calendar";

	}

	public function labelCard6AmazingFeatures(){

		if(trim($this->labelCard6AmazingFeatures) != null){

			return $this->labelCard6AmazingFeatures;

		}

		return "Touch ID";

	}

	public function detailCard1AmazingFeatures(){

		if(trim($this->detailCard1AmazingFeatures) != null){

			return $this->detailCard1AmazingFeatures;

		}

		return "Engage users with up to date information, seasonal specials, new products or services.";

	}



	public function detailCard2AmazingFeatures(){

		if(trim($this->detailCard2AmazingFeatures) != null){

			return $this->detailCard2AmazingFeatures;

		}

		return "You take patient data and so do we. Pulse is purpose-built to follow HIPAA and HITECH guidelines.";

	}


	public function detailCard3AmazingFeatures(){

		if(trim($this->detailCard3AmazingFeatures) != null){

			return $this->detailCard3AmazingFeatures;

		}

		return "Tailor content to visitors based on where they are and encourage them to take action.";

	}



	public function detailCard4AmazingFeatures(){

		if(trim($this->detailCard4AmazingFeatures) != null){

			return $this->detailCard4AmazingFeatures;

		}

		return "Leverage the power of video to provide valuable information, insights, and promotions.";

	}


	public function detailCard5AmazingFeatures(){

		if(trim($this->detailCard5AmazingFeatures) != null){

			return $this->detailCard5AmazingFeatures;

		}

		return "Having a special event? Market it with the Events sections and increase buzz and attendance.";

	}

	public function detailCard6AmazingFeatures(){

		if(trim($this->detailCard6AmazingFeatures) != null){

			return $this->detailCard6AmazingFeatures;

		}

		return "Add an extra level of personal security and ease for your users by enabling TouchID";

	}

	public function labelPricing(){

		if(trim($this->labelPricing) != null){

			return $this->labelPricing;

		}

		return "Pricing";

	}


	public function labelPricePulse(){

		if(trim($this->labelPricePulse) != null){

			return $this->labelPricePulse;

		}

		return "497";

	}

	public function labelFeatureValuePulse(){

		if(trim($this->labelFeatureValuePulse) != null){

			return $this->labelFeatureValuePulse;

		}

		return "$2,500";


	}


	public function labelFeatureInfoPulse(){

		if(trim($this->labelFeatureInfoPulse) != null){

			return $this->labelFeatureInfoPulse;

		}

		return "One Time Set Up Fee";


	}





	public function labelPricePulseMvp(){


		if(trim($this->labelPricePulseMvp) != null){

			return $this->labelPricePulseMvp;

		}

		return "997";


	}


	public function labelFeatureValuePulseMvp(){


		if(trim($this->labelFeatureValuePulseMvp) != null){

			return $this->labelFeatureValuePulseMvp;

		}

		return "$5,000";


	}


	public function labelFeatureInfoPulseMvp(){


		if(trim($this->labelFeatureInfoPulseMvp) != null){

			return $this->labelFeatureInfoPulseMvp;

		}

		return "One Time Set Up Fee";


	}

	public function labelCaptionPulseEnterprise(){

		if(trim($this->labelCaptionPulseEnterprise) != null){

			return $this->labelCaptionPulseEnterprise;

		}

		return "Go Beyond the Next Level";


	}


	public function labelButtonLabelPulse(){

		if(trim($this->labelButtonLabelPulse) != null){

			return $this->labelButtonLabelPulse;

		}

		return "Let's Talk";


	}


	public function labelButtonLabelLinkPulse(){

		if(trim($this->labelButtonLabelLinkPulse) != null){

			return $this->labelButtonLabelLinkPulse;

		}

		return "#";

	}


	public function labelButtonLabelPulseMvp(){

		if(trim($this->labelButtonLabelPulseMvp) != null){

			return $this->labelButtonLabelPulseMvp;

		}

		return "Let's Talk";


	}


	public function labelButtonLabelLinkPulseMvp(){

		if(trim($this->labelButtonLabelLinkPulseMvp) != null){

			return $this->labelButtonLabelLinkPulseMvp;

		}

		return "#";

	}



	public function labelButtonLabelPulseEnterprise(){

		if(trim($this->labelButtonLabelPulseEnterprise) != null){

			return $this->labelButtonLabelPulseEnterprise;

		}

		return "Let's Talk";

	}


	public function labelButtonLabelLinkPulseEnterprise(){

		if(trim($this->labelButtonLabelLinkPulseEnterprise) != null){

			return $this->labelButtonLabelLinkPulseEnterprise;

		}

		return "#";

	}


	public function labelBottomButtonLabelPulseEnterprise(){

		if(trim($this->labelBottomButtonLabelPulseEnterprise) != null){

			return $this->labelBottomButtonLabelPulseEnterprise;

		}

		return "Read The Enteprise Case Study";

	}


	public function labelBottomButtonLinkLabelPulseEnterprise(){

		if(trim($this->labelBottomButtonLinkLabelPulseEnterprise) != null){

			return $this->labelBottomButtonLinkLabelPulseEnterprise;

		}

		return "#";

	}


	public function labelCTACaption(){


		if(trim($this->labelCTACaption) != null){

			return $this->labelCTACaption;

		}

		return "Ready to have your own iOS and Android app?";


	}

	public function labelCTAButtonLabel(){

		if(trim($this->labelCTAButtonLabel) != null){

			return $this->labelCTAButtonLabel;

		}

		return "Talk to us";

	}


	public function labelCTAButtonLink(){

		if(trim($this->labelCTAButtonLink) != null){

			return $this->labelCTAButtonLink;

		}

		return "#";

	}


	







}