<?php

use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

use SilverStripe\Forms\TextField;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;


use Contact;
use Receiver;

class ContactPage extends Page{

	private static $db = [
		'dbLabel' => 'Text',
		'dbDetails' => 'Text',
		'dbCompanyName' => 'Text',
		'dbAddress' => 'Text'
	];

	private static $has_one = [
		'dbBackgroundImage' => Image::class
	];

	private static $has_many = [
		'Contact' => Contact::class,
		'Receiver' => Receiver::class
	];


	public function getCMSFields(){

		$fields = parent::getCMSFields();

		$fields->addFieldToTab(
			'Root.Main',
   	 		TextField::create('dbLabel',"Left Caption")->setDescription("Maximum of 30 characters including spaces.")->setMaxLength(30),
   	 		'Content'
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Main',
   	 		TextField::create('dbDetails',"Left Details")->setDescription("Maximum of 100 characters including spaces.")->setMaxLength(100),
   	 		'Content'
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Main',
   	 		TextField::create('dbCompanyName',"Company Name")->setDescription("Maximum of 30 characters including spaces.")->setMaxLength(30),
   	 		'Content'
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Main',
   	 		TextField::create('dbAddress',"Company Address")->setDescription("Maximum of 30 characters including spaces.")->setMaxLength(100),
   	 		'Content'
   	 	);


   	 	// remove the add button in grid field
		$config = GridFieldConfig_RecordEditor::create();
	   	$config->removeComponentsByType(GridFieldAddNewButton::class);

	   	$fields->addFieldToTab('Root.Main', 
            new GridField('Contact', 'All Inquiries', $this->Contact(),$config),
            'Content'
        );


	   	$fields->addFieldToTab(
	   		'Root.Main', 
            new GridField('Receiver', 'List of Email Receivers', $this->Receiver(),GridFieldConfig_RecordEditor::create()),
            'Content'
        );

   	 	return $fields;

	}



}