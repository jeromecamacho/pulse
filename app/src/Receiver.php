<?php

use SilverStripe\ORM\DataObject;

use ContactPage;

class Receiver extends DataObject{


	private static $singular_name = "Email Receiver";


	private static $db = [
		'db_email' => 'Text',
	];

	private static $field_labels = array(
		'db_email' => 'Email',
	);

	private static $summary_fields = [
		'db_email' => 'Email',
	];

	private static $has_one = [
		'ContactPage' => ContactPage::class
	];

}
