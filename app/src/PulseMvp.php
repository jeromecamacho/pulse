<?php

use SilverStripe\ORM\DataObject;

use Main\HomePage;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;


class PulseMvp extends DataObject{

	private static $singular_name = "Pulse MVP Details";
	// private static $plural_name = "Add Extra Product Gallery Images";

	private static $db = [
		'dbLabel' => 'Text'
	];

	private static $summary_fields = [
		'dbLabel' => "Name"
	];

	private static $has_one = [
		"HomePage" => HomePage::class
	];

	public function getCMSFields(){

        $fields = FieldList::create(
            TextField::create('dbLabel','Name')
        );

		return $fields;

	}

}
