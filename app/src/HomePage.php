<?php

namespace Main;

use Page;

use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

use SilverStripe\Forms\TextField;
use SilverStripe\Forms\NumericField;

use SilverStripe\Forms\TextareaField;


use SilverStripe\Forms\HeaderField;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;


use Pulse;
use PulseEnterprise;
use PulseMvp;

class HomePage extends Page{

	private static $db = [

		// header
		'caption1' => 'Text',
		'caption2' => 'Text',
		'buttonCaptionHeader' => 'Text',


		// we focus
		'label1_weFocus' => 'Text',
		'label2_weFocus' => 'Text',
		'buttonLabelWeFocus' => 'Text',

		//push notifications
		'label1PushNotif' => 'Text',
		'details1PushNotif' => 'Text',
		'label2PushNotif' => 'Text',
		'details2PushNotif' => 'Text',


		// real time appointments
		'labelRealTimeAppointments' => 'Text',
		'detailRealTimeAppointments' => 'Text',

		// pulse dashboard
		'labelPulseDashboard' => 'Text',
		'detailsPulseDashboard' => 'Text',
		'label2PulseDashboard' => 'Text',

		// patient checkin
		'label1PatientCheckin' => 'Text',
		'label2PatientCheckin' => 'Text',
		'detailsPatientCheckin' => 'Text',
		'label3PatientCheckin' => 'Text',
		'details2PatientCheckin' => 'Text',


		// electronic clipboard
		'label1EClipboard' => 'Text',
		'label2EClipboard' => 'Text',
		'detailsEClipboard' => 'Text',


		// other amazing features
		'labelAmazingFeatures' => 'Text',

		'labelCard1AmazingFeatures' => 'Text',
		'detailCard1AmazingFeatures' => 'Text',

		'labelCard2AmazingFeatures' => 'Text',
		'detailCard2AmazingFeatures' => 'Text',

		'labelCard3AmazingFeatures' => 'Text',
		'detailCard3AmazingFeatures' => 'Text',

		'labelCard4AmazingFeatures' => 'Text',
		'detailCard4AmazingFeatures' => 'Text',

		'labelCard5AmazingFeatures' => 'Text',
		'detailCard5AmazingFeatures' => 'Text',

		'labelCard6AmazingFeatures' => 'Text',
		'detailCard6AmazingFeatures' => 'Text',

		// pricing
		'labelPricing' => 'Text',

		'labelPricePulse' => 'Text',
		'labelFeatureValuePulse' => 'Text',
		'labelFeatureInfoPulse' => 'Text',
		'labelButtonLabelPulse' => 'Text',
		'labelButtonLabelLinkPulse' => 'Text',

		'labelPricePulseMvp' => 'Text',
		'labelFeatureValuePulseMvp' => 'Text',
		'labelFeatureInfoPulseMvp' => 'Text',
		'labelButtonLabelPulseMvp' => 'Text',
		'labelButtonLabelLinkPulseMvp' => 'Text',


		'labelCaptionPulseEnterprise' => 'Text',
		'labelButtonLabelPulseEnterprise' => 'Text',
		'labelButtonLabelLinkPulseEnterprise' => 'Text',
		'labelBottomButtonLabelPulseEnterprise' => 'Text',
		'labelBottomButtonLinkLabelPulseEnterprise' => 'Text',


		// CTA
		'labelCTACaption' => 'Text',
		'labelCTAButtonLabel' => 'Text',
		'labelCTAButtonLink' => 'Text'



	];

	private static $has_one = [

		// header
		'headerBackground' => Image::class,

		// we focus
		'weFocusIcon' => Image::class,
		'weFocusImageBottom' => Image::class,

		//push notifications
		'imageRightPushNotif' => Image::class,

		// real time appointments
		'imageRightRealTimeAppointments' => Image::class,

		// pulse dashboard
		'imageRightPulseDashboard' => Image::class,

		// patient checkin
		'imageLeftPatientCheckin' => Image::class,

		// electronic clipboard
		'imageRightEClipboard' => Image::class,

		// CTA
		'imageIconCTA' => Image::class,
		'imageBackgroundCTA' => Image::class,


	];


	private static $has_many = [
		'Pulse' => Pulse::class,
		'PulseMvp' => PulseMvp::class ,
		'PulseEnterprise' => PulseEnterprise::class ,
	];



	private static $owns = [

		// header
		'headerBackground',

		//we focus
		'weFocusIcon',
		'weFocusImageBottom',

		//push notifications
		'imageRightPushNotif',

		// real time appointments
		'imageRightRealTimeAppointments',

		// pulse dashboard
		'imageRightPulseDashboard',

		// patient checkin
		'imageLeftPatientCheckin',

		// electronic clipboard
		'imageRightEClipboard',

		//CTA
		'imageIconCTA',
		'imageBackgroundCTA',





	];


	public function getCMSFields(){

	    $fields = parent::getCMSFields();

	    // header
	    $fields->addFieldToTab(
			'Root.Header Section',
   	 		TextField::create('caption1',"Caption 1")->setMaxLength(50)->setDescription('Maximum of 50 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Header Section',
   	 		TextField::create('caption2',"Caption 2")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Header Section',
   	 		TextField::create('buttonCaptionHeader',"Button Label")->setMaxLength(20)->setDescription('Maximum of 20 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.Header Section',
            $headerBackground = UploadField::create('headerBackground','Header Background')->setDescription("Only png, jpg, jpeg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 2000px and height: 1000px)"),
            ''
        );
        $headerBackground->getValidator()->setAllowedExtensions(['png','jpg','jpeg']);
        $headerBackground->getValidator()->setMinDimensions(2000,1000);
        $headerBackground->setAllowedMaxFileNumber(1);
        $headerBackground->setFolderName('Uploads/Header_Background');
   	 	// end of header


   	 	// we focus section
        $fields->addFieldToTab(
            'Root.We Focus Section',
            $weFocusIcon = UploadField::create('weFocusIcon','Header Background')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 140px and height: 60px)"),
            ''
        );
        $weFocusIcon->getValidator()->setAllowedExtensions(['png']);
        $weFocusIcon->getValidator()->setMinDimensions(140,60);
        $weFocusIcon->setAllowedMaxFileNumber(1);
        $weFocusIcon->setFolderName('Uploads/weFocusIcon');


        $fields->addFieldToTab(
			'Root.We Focus Section',
   	 		TextField::create('label1_weFocus',"Label 1")->setMaxLength(50)->setDescription('Maximum of 50 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.We Focus Section',
   	 		TextField::create('label2_weFocus',"Details 1")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.We Focus Section',
   	 		TextField::create('buttonLabelWeFocus',"Button Label")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


        $fields->addFieldToTab(
            'Root.We Focus Section',
            $weFocusImageBottom = UploadField::create('weFocusImageBottom','Bottom Image')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 262px and height: 290px)"),
            ''
        );
        $weFocusImageBottom->getValidator()->setAllowedExtensions(['png']);
        $weFocusImageBottom->getValidator()->setMinDimensions(262,290);
        $weFocusImageBottom->setAllowedMaxFileNumber(1);
        $weFocusImageBottom->setFolderName('Uploads/weFocusImageBottom');
   	 	//end of we focus section


   	 	// push notifcation
   	 	$fields->addFieldToTab(
			'Root.Push Notification Section',
   	 		TextField::create('label1PushNotif',"Label 1")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Push Notification Section',
   	 		TextField::create('details1PushNotif',"Details 1")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Push Notification Section',
   	 		TextField::create('label2PushNotif',"Label 2")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Push Notification Section',
   	 		TextField::create('details2PushNotif',"Details 2")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.Push Notification Section',
            $imageRightPushNotif = UploadField::create('imageRightPushNotif','Right Image')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 545px and height: 595px)"),
            ''
        );
        $imageRightPushNotif->getValidator()->setAllowedExtensions(['png']);
        $imageRightPushNotif->getValidator()->setMinDimensions(545,595);
        $imageRightPushNotif->setAllowedMaxFileNumber(1);
        $imageRightPushNotif->setFolderName('Uploads/imageRightPushNotif');
   	 	//end of push notification

   	 	// realtime appointments
   	 	$fields->addFieldToTab(
			'Root.Real Time Appointments Section',
   	 		TextField::create('labelRealTimeAppointments',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Real Time Appointments Section',
   	 		TextField::create('detailRealTimeAppointments',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
            'Root.Real Time Appointments Section',
            $imageRightRealTimeAppointments = UploadField::create('imageRightRealTimeAppointments','Left Image')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 527px and height: 571px)"),
            ''
        );
        $imageRightRealTimeAppointments->getValidator()->setAllowedExtensions(['png']);
        $imageRightRealTimeAppointments->getValidator()->setMinDimensions(527,571);
        $imageRightRealTimeAppointments->setAllowedMaxFileNumber(1);
        $imageRightRealTimeAppointments->setFolderName('Uploads/imageRightRealTimeAppointments');

        // end of realtime appointments


        // pulse dashboard
        $fields->addFieldToTab(
			'Root.Pulse Dashboard Section',
   	 		TextField::create('labelPulseDashboard',"Label 1")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Pulse Dashboard Section',
   	 		TextField::create('detailsPulseDashboard',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Pulse Dashboard Section',
   	 		TextField::create('label2PulseDashboard',"Label 2")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.Pulse Dashboard Section',
            $imageRightPulseDashboard = UploadField::create('imageRightPulseDashboard','Right Image')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 725px and height: 419px)"),
            ''
        );
        $imageRightPulseDashboard->getValidator()->setAllowedExtensions(['png']);
        $imageRightPulseDashboard->getValidator()->setMinDimensions(725,419);
        $imageRightPulseDashboard->setAllowedMaxFileNumber(1);
        $imageRightPulseDashboard->setFolderName('Uploads/imageRightPulseDashboard');
        // end of pulse dashboard


        // patient checkin
		$fields->addFieldToTab(
			'Root.Patient Check-in Section',
   	 		TextField::create('label1PatientCheckin',"Label 1")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Patient Check-in Section',
   	 		TextField::create('label2PatientCheckin',"Label 2")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Patient Check-in Section',
   	 		TextField::create('detailsPatientCheckin',"Details 1")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Patient Check-in Section',
   	 		TextField::create('label3PatientCheckin',"Label 3")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Patient Check-in Section',
   	 		TextField::create('details2PatientCheckin',"Details 2")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.Patient Check-in Section',
            $imageLeftPatientCheckin = UploadField::create('imageLeftPatientCheckin','Left Image')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 534px and height: 519px)"),
            ''
        );
        $imageLeftPatientCheckin->getValidator()->setAllowedExtensions(['png']);
        $imageLeftPatientCheckin->getValidator()->setMinDimensions(534,519);
        $imageLeftPatientCheckin->setAllowedMaxFileNumber(1);
        $imageLeftPatientCheckin->setFolderName('Uploads/imageLeftPatientCheckin');
        // end of patient checkin


        // electronic clipboard
        $fields->addFieldToTab(
			'Root.Electronic Clipboard Section',
   	 		TextField::create('label1EClipboard',"Label 1")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Electronic Clipboard Section',
   	 		TextField::create('label2EClipboard',"Label 2")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Electronic Clipboard Section',
   	 		TextField::create('detailsEClipboard',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.Electronic Clipboard Section',
            $imageRightEClipboard = UploadField::create('imageRightEClipboard','Right Image')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 667px and height: 451px)"),
            ''
        );
        $imageRightEClipboard->getValidator()->setAllowedExtensions(['png']);
        $imageRightEClipboard->getValidator()->setMinDimensions(667,451);
        $imageRightEClipboard->setAllowedMaxFileNumber(1);
        $imageRightEClipboard->setFolderName('Uploads/imageRightEClipboard');
        // end of electronic clipboard



        // other amazing features
        $fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('labelAmazingFeatures',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
			new HeaderField(" ",'Card 1',1),
			''
		);

		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('labelCard1AmazingFeatures',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('detailCard1AmazingFeatures',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);



		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
			new HeaderField(" ",'Card 2',1),
			''
		);


		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('labelCard2AmazingFeatures',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('detailCard2AmazingFeatures',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);




		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
			new HeaderField(" ",'Card 3',1),
			''
		);

		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('labelCard3AmazingFeatures',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('detailCard3AmazingFeatures',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
			new HeaderField(" ",'Card 4',1),
			''
		);

		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('labelCard4AmazingFeatures',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('detailCard4AmazingFeatures',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);



		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
			new HeaderField(" ",'Card 5',1),
			''
		);

		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('labelCard5AmazingFeatures',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('detailCard5AmazingFeatures',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);



		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
			new HeaderField(" ",'Card 6',1),
			''
		);

		$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('labelCard6AmazingFeatures',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Other Amazing Features Section',
   	 		TextField::create('detailCard6AmazingFeatures',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.'),
   	 		''
   	 	);


   	 	// pricings
   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelPricing',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
   	 		''
   	 	);




   	 	// PULSE
   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
			new HeaderField(" ",'Pulse',1),
			''
		);

		$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelPricePulse',"Price"),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
			new HeaderField(" ",'Pulse Features',4),
			''
		);

		$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelFeatureValuePulse',"Price"),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelFeatureInfoPulse',"Details"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.Pricing Section',
            GridField::create('Pulse','List of Pulse Details',$this->Pulse(),GridFieldConfig_RecordEditor::create()),
            ''
        );


        $fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelButtonLabelPulse',"Button Label"),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelButtonLabelLinkPulse',"Redirect To")->setDescription("http://sample.com"),
   	 		''
   	 	);

		



   	 	// PULSE MVP
   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
			new HeaderField(" ",'Pulse MVP',1),
			''
		);

		$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelPricePulseMvp',"Price"),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
			new HeaderField(" ",'Pulse MVP Features',4),
			''
		);

		$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelFeatureValuePulseMvp',"Price"),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelFeatureInfoPulseMvp',"Details"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.Pricing Section',
            GridField::create('PulseMvp','List of Pulse MVP Details',$this->PulseMvp(),GridFieldConfig_RecordEditor::create()),
            ''
        );


        $fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelButtonLabelPulseMvp',"Button Label"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelButtonLabelLinkPulseMvp',"Redirect To")->setDescription("http://sample.com"),
   	 		''
   	 	);



   	 	// pulse enterprise
        $fields->addFieldToTab(
			'Root.Pricing Section',
			new HeaderField(" ",'Pulse Enterprise Features',1),
			''
		);


		$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelCaptionPulseEnterprise',"Details"),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
            'Root.Pricing Section',
            GridField::create('PulseEnterprise','List of Pulse Enterprise Details',$this->PulseEnterprise(),GridFieldConfig_RecordEditor::create()),
            ''
        );


        $fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelButtonLabelPulseEnterprise',"Button Label"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelButtonLabelLinkPulseEnterprise',"Redirect To")->setDescription("http://sample.com"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelBottomButtonLabelPulseEnterprise',"Button Label in Bottom"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.Pricing Section',
   	 		TextField::create('labelBottomButtonLinkLabelPulseEnterprise',"Redirect To")->setDescription("http://sample.com"),
   	 		''
   	 	);



   	 	// CTA
   	 	$fields->addFieldToTab(
            'Root.CTA Section',
            $imageIconCTA = UploadField::create('imageIconCTA','Header Background')->setDescription("Only png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 63px and height: 68px)"),
            ''
        );
        $imageIconCTA->getValidator()->setAllowedExtensions(['png']);
        $imageIconCTA->getValidator()->setMinDimensions(63,68);
        $imageIconCTA->setAllowedMaxFileNumber(1);
        $imageIconCTA->setFolderName('Uploads/imageIconCTA');



   	 	$fields->addFieldToTab(
			'Root.CTA Section',
   	 		TextField::create('labelCTACaption',"Label")->setDescription("Maximum of 30 characters including spaces.")->setMaxLength(30),
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.CTA Section',
   	 		TextField::create('labelCTAButtonLabel',"Button Label")->setDescription("Maximum of 30 characters including spaces.")->setMaxLength(30),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
			'Root.CTA Section',
   	 		TextField::create('labelCTAButtonLink',"Redirect To")->setDescription("http://sample.com"),
   	 		''
   	 	);


   	 	$fields->addFieldToTab(
            'Root.CTA Section',
            $imageBackgroundCTA = UploadField::create('imageBackgroundCTA','Header Background')->setDescription("Only png, jpeg, jpg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 2000px and height: 1000px)"),
            ''
        );
        $imageBackgroundCTA->getValidator()->setAllowedExtensions(['png','jpeg','jpg']);
        $imageBackgroundCTA->getValidator()->setMinDimensions(2000,1000);
        $imageBackgroundCTA->setAllowedMaxFileNumber(1);
        $imageBackgroundCTA->setFolderName('Uploads/imageBackgroundCTA');

        









	    return $fields;
	}



}