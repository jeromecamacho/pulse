<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;

    use SilverStripe\View\Requirements;

    use FooterPage;

    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = [];

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/

            Requirements::css("css/bootstrap.min.css");
            Requirements::css("css/animate.min.css");
            Requirements::css("css/font-awesome.min.css");
            Requirements::css("fonts/metropolis/stylesheet.css");
            Requirements::css("fonts/ionicons/stylesheet.css");
            Requirements::css("css/themify-icons.css");
            Requirements::css("css/YTPlayer.css");
            Requirements::css("css/owl.carousel.css");
            Requirements::css("css/magnific-popup.css");
            Requirements::css("css/main.css");
            Requirements::css("css/responsive.css");
            Requirements::css("css/custom.css");



            Requirements::javascript("js/jquery-2.1.4.min.js");
            Requirements::javascript("js/bootstrap.min.js");
            Requirements::javascript("js/jquery.fitvids.js");
            Requirements::javascript("js/jquery.mb.YTPlayer.js");
            Requirements::javascript("js/owl.carousel.min.js");
            Requirements::javascript("js/wow.min.js");
            Requirements::javascript("js/jquery.parallax-1.1.3.js");
            Requirements::javascript("js/jquery.countTo.js");
            Requirements::javascript("js/jquery.countdown.min.js");
            Requirements::javascript("js/jquery.appear.js");
            Requirements::javascript("js/smoothscroll.js");
            Requirements::javascript("js/jquery.magnific-popup.min.js");
            Requirements::javascript("js/imagesloaded.pkgd.min.js");
            Requirements::javascript("js/isotope.pkgd.min.js");
            Requirements::javascript("js/email.js");
            Requirements::javascript("js/main.js");

            Requirements::customCSS("

                .hero-caption.caption-center {
                    left: 0;
                    right: 0;
                    text-align: left;
                }

            ");

        }


        public function getFooterEmail(){

            $getFooter = FooterPage::get()->first();
            
            $dbEmail = $getFooter->dbEmail;

            if(trim($dbEmail) != null){

                return $dbEmail;

            }

            return "hello@pulsemvp.com";

        }


        public function getFooterAddress(){

            $getFooter = FooterPage::get()->first();
            
            $dbAddress = $getFooter->dbAddress;

            if(trim($dbAddress) != null){

                return $dbAddress;

            }

            return "1551 Forum Place Suite 500A "."\n"."West Palm Beach, FL 33401";

        }



        public function getFooterPhone(){

            $getFooter = FooterPage::get()->first();
            
            $dbPhone = $getFooter->dbPhone;

            if(trim($dbPhone) != null){

                return $dbPhone;

            }

            return "(561) 814-5742";

        }



        public function getFooterCopyrightDetails(){

            $getFooter = FooterPage::get()->first();
            
            $dbCopyrightDetails = $getFooter->dbCopyrightDetails;

            if(trim($dbCopyrightDetails) != null){

                return $dbCopyrightDetails;

            }

            return "2018 Pulse Inc., All Rights Reserved.";

        }




        public function getFooterFacebookLink(){

            $getFooter = FooterPage::get()->first();
            
            $dbFacebookLink = $getFooter->dbFacebookLink;

            if(trim($dbFacebookLink) != null){

                return $dbFacebookLink;

            }

            return "#";

        }



        public function getFooterTwitterLink(){

            $getFooter = FooterPage::get()->first();
            
            $dbTwitterLink = $getFooter->dbTwitterLink;

            if(trim($dbTwitterLink) != null){

                return $dbTwitterLink;

            }

            return "#";

        }



        public function getFooterInstagramLink(){

            $getFooter = FooterPage::get()->first();
            
            $getFooterInstagramLink = $getFooter->getFooterInstagramLink;

            if(trim($getFooterInstagramLink) != null){

                return $getFooterInstagramLink;

            }

            return "#";

        }


       



    }
}
