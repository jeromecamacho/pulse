<?php

use SilverStripe\Forms\TextField;

use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\RequiredFields;


use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;


use SilverStripe\Control\Email\SwiftMailer;
use SilverStripe\Control\Email\Email;
use SilverStripe\Core\Config\Config;

use Contact;
use Receiver;


class ContactPageController extends PageController{

	private static $allowed_actions = [
		'submit_contact_query',
	];


	
	public function dbLabel(){

		if(trim($this->dbLabel) != null){

			return $this->dbLabel;

		}

		return "Get started with a healthcare mobile app for your business"; 

	}


	public function dbDetails(){

		if(trim($this->dbDetails) != null){

			return $this->dbDetails;
			
		}

		return "Our simple to use software and mobile app provide a seamless solution for your visitors and staff."; 

	}

	public function dbCompanyName(){


		if(trim($this->dbCompanyName) != null){

			return $this->dbCompanyName;
			
		}

		return "Direct Works Media"; 



	}


	public function dbAddress(){

		if(trim($this->dbAddress) != null){

			return $this->dbAddress;

		}


		return "1551 Forum Place
				Suite 500A
                West Palm Beach, FL  33401";

	}



    public function getCurrentLink() {
        return Director::get_current_page()->Link()."submit_contact_query";
    }



	public function ContactForm(){

		$fields = new FieldList(

		);

		$actions = new FieldList(
			new FormAction('submit_contact_query', 'Submit')
		);

		$form = new Form($this, 'submit_contact_query', $fields, $actions);

		$actions = $form->Actions();

		$form->setTemplate('Includes/ContactForm');
		$form->setActions($actions);

		
		return $form->customise(
			[
		    	'getCurrentLink' => $this->getCurrentLink()
			]
		);


	}



	public function submit_contact_query(HTTPRequest $request){

        $data = $request->requestVars();


        // captcha verification backend
		$captcha_data = array(
            'secret' => '6Leo9n8UAAAAADRo1GgjMDEVL0VdPRvdktkIWVCK',
            'response' => $data['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        );
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $captcha_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $result = json_decode($response,true);
        curl_close($ch);
        $json_response = json_decode($response);

        $return_value = [];

        if($result['success'] != true) {

           $return_value['valid'] = "Invalid Recaptcha";

        }else{

	        //value from form
	        $fname = $data['fname'];
	        $lname = $data['lname'];
	        $email = $data['email'];
	        $phone = $data['phone'];
	        $company = $data['cname'];
	        $comments = $data['company'];

	        // hubspot
	        $arr = array(
					            'properties' => array(
					                array(
					                    'property' => 'email',
					                    'value' => $email
					                ),
					                array(
					                    'property' => 'firstname',
					                    'value' => $fname
					                ),
					                array(
					                    'property' => 'lastname',
					                    'value' => $lname
					                ),
					                array(
					                    'property' => 'phone',
					                    'value' => $phone
					                )
					            )
					    );

			$post_json = json_encode($arr);

			// $hapikey = "4cb4adf5-0fe3-4bd7-866d-1d54c7569bb9";

			// sir lu hubspot
			$hapikey = "8f0e2ce8-44b4-44e5-b7e8-3fe584310e49";

			$endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
			$ch = @curl_init();
			@curl_setopt($ch, CURLOPT_POST, true);
			@curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
			@curl_setopt($ch, CURLOPT_URL, $endpoint);
			@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = @curl_exec($ch);
			$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$curl_errors = curl_error($ch);
			@curl_close($ch);
			// end of hubspot


			if($status_code == 200){

				// sender
				$from = "jerome@directworksmedia.com";

				//main receiver
				$to = "lu@directworksmedia.com";
				// $to = "jerome@directworksmedia.com";

				$subject = "Contact Form Mailer";

				$body = "";
				$body .= "First Name : ".$fname."<br>";
				$body .= "Last Name : ".$lname."<br>";
				$body .= "Email : ".$email."<br>";
				$body .= "Phone : ".$phone."<br>";
				$body .= "Company Name : ".$company."<br>";
				$body .= "Comments : ".$comments."<br>";


				$email_send = new Email($from, $to, $subject, $body);
				$email_send->setFrom($from,"Contact Form Mailer");

				// the receiver is set by the list of receiver in admin cms in contact page
				$current_receiver = Receiver::get()
									->filter(
										[
											"ContactPageID" => $this->ID
										]
									);

				foreach ($current_receiver as $each_receiver) {

					$email_send->addCC($each_receiver->db_email);

				}

				if(!$email_send->send()) {

					$return_value["valid"] = "email_not_send";

				} else {

					// saving in db
					$saveContact = Contact::create();
					$saveContact->dbFirstname = $fname;
					$saveContact->dbLastname = $lname;
					$saveContact->dbEmail = $email;
					$saveContact->dbPhone = $phone;
					$saveContact->dbCompany = $company;
					$saveContact->dbComments = $comments;
					$saveContact->ContactPageID = $this->ID;
					$saveContact->write();

					$return_value["valid"] = "success";

				}

			//invalid hubspot integration
			}else{


				// show the message from hubspot response
				$json_decode = json_decode($response);

	           	$return_value['valid'] = $json_decode->message;


			}

        }

	    return json_encode($return_value);

	}




}