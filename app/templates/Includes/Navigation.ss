<%-- <nav class="primary">
	<span class="nav-open-button">²</span>
	<ul>
		<% loop $Menu(1) %>
			<li class="$LinkingMode"><a href="$Link" title="$Title.XML">$MenuTitle.XML</a></li>
		<% end_loop %>
	</ul>
</nav>
 --%>



<!-- Navigation -->
    <nav class="main-nav menu-dark menu-transparent js-transparent">
        <div class="container">
            <div class="navbar">
                <div class="brand-logo">
                    <a class="navbar-brand" href="$BaseRef">
                        <img src="images/logo/pulse-logo-w.svg" alt="Pulse">
                    </a>
                </div>
                <!-- brand-logo -->
                 
                <!-- navbar-header -->
                <div class="navbar-header">
                    <div class="inner-nav right-nav">
                        <ul>
                            <!-- /dropdown -->
                            <li class="navbar-toggle">
                                <button type="button" data-toggle="collapse" data-target=".collapse">
                                    <span class="sr-only"></span>
                                    <i class="ti-menu"></i>
                                </button>
                            </li>
                            <!-- /collapse-menu -->
                        </ul>
                    </div>
                    <!-- /right-nav -->
                </div>
                <div class="custom-collapse navbar-collapse collapse inner-nav">
                    <ul class="nav navbar-nav">
                        <%-- <li><a href="#about" class="scroll-to">About </a> </li>
                        <li><a href="#features" class="scroll-to">Features </a> </li>
                        <li><a href="#pricing" class="scroll-to">Pricing </a> </li>
                        <li><a href="contact.html" class="scroll-to">Contact </a> </li> --%>

                        <% loop $Menu(1) %>

                            <% if $MenuTitle == "Features"%>

                                 <li class="scroll-to"><a href="$BaseURL#features" title="$Title.XML" >$MenuTitle.XML</a></li>


                            <% else_if $MenuTitle == "Pricing"%>

                                 <li class="scroll-to"><a href="$BaseURL#pricing" title="$Title.XML" >$MenuTitle.XML</a></li>

                            <% else %>

							     <li class="scroll-to $LinkingMode"><a href="$Link" title="$Title.XML" >$MenuTitle.XML</a></li>

                            <% end_if %>

						<% end_loop %>
                    </ul>
                    <!-- /nav -->
                </div>
                <!-- /collapse -->
            </div>
            <!-- /navbar -->
        </div>
        <!-- /container -->
    </nav>
    <!--/#Navigation-->