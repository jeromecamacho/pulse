

<!DOCTYPE html>
<!--
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Simple. by Sara (saratusar.com, @saratusar) for Innovatif - an awesome Slovenia-based digital agency (innovatif.com/en)
Change it, enhance it and most importantly enjoy it!
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->

<!--[if !IE]><!-->
<html lang="$ContentLocale">
<!--<![endif]-->
<!--[if IE 6 ]><html lang="$ContentLocale" class="ie ie6"><![endif]-->
<!--[if IE 7 ]><html lang="$ContentLocale" class="ie ie7"><![endif]-->
<!--[if IE 8 ]><html lang="$ContentLocale" class="ie ie8"><![endif]-->
<head>
    <% base_tag %>
    <title><% if $MetaTitle %>$MetaTitle<% else %>$Title<% end_if %> &raquo; $SiteConfig.Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link rel="shortcut icon" href="images/logo/favicon.png">

    $MetaTags(false)
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <%-- <% require themedCSS('reset') %> --%>
    <%-- <% require themedCSS('typography') %> --%>
    <%-- <% require themedCSS('form') %> --%>
    <%-- <% require themedCSS('layout') %> --%>
    <link rel="shortcut icon" href="themes/simple/images/favicon.ico" />
</head>

<body class="$ClassName.ShortName<% if not $Menu(2) %> no-sidebar<% end_if %>" <% if $i18nScriptDirection %>dir="$i18nScriptDirection"<% end_if %>>

    <% include Header %>
    <% include Navigation %>


    <!-- Contact -->
        <div class="bg-pattern contact  fs-img-parallax parallax" >
            <div class="wrap hero-caption caption-center caption-height-center container">
                <div class="row-centered">
                    <div class="col-md-5 col-centered">
                        <a href="#">
                            <img srcset="images/pulse/success.png" alt="Pulse">
                        </a>
                        <h1 class="mrg-btm-20 text-shadow">
                            Great!</h1>
                        <p class="mrg-btm-40">We received your message! Will get back to you shortly.</p>
                    </div>
                     
                </div>
            </div>
        </div>
        <!-- /Contact -->

    
    <% include Footer %>

<%-- <% require javascript('//code.jquery.com/jquery-3.3.1.min.js') %> --%>
<%-- <% require themedJavascript('script') %> --%>

</body>
</html>
