<?php
 
use SilverStripe\Forms\HTMLEditor\HtmlEditorConfig;

HtmlEditorConfig::get('cms')->setOption(
    'extended_valid_elements',
    '*[*]'
);

HtmlEditorConfig::get('cms')->setOption(
    'valid_children',
    '*[*]'
);